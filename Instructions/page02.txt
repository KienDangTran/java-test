this though, please specify the new compliance    
level of the project.                             
                                                  
        You are not allowed to use third party    
libraries. But if you know of libraries that would
have helped you create a good solution, then      
please mention them and motivate how it would have
helped you. However, you are free to use a unit   
test library (like JUnit or TestNG) if you work   
best with TDD. In this case, your tests will also 
be used to measure the quality of your code.      
                                                  
                                            page 2