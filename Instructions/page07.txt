__________________The assignment__________________
                                                  
       Your assignment is to add support for      
replaceable text-parts. This is how it should     
work.                                             
       The program reads an additional            
property-file where some words are kept in the    
following format:                                 
hero=Hercules                                     
villan=The Joker                                  
                                                  
       An xml-file with text like "${hero}        
                                            page 7