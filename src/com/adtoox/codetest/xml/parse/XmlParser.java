package com.adtoox.codetest.xml.parse;

import com.adtoox.codetest.xml.IXmlTag;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.Reader;

public class XmlParser implements IXmlParser {
	private SAXParser saxParser;

	public XmlParser() throws ParserConfigurationException, SAXException {
		this.saxParser = SAXParserFactory.newInstance().newSAXParser();
	}

	@Override
	public IXmlTag parse(Reader reader) throws SAXException, IOException {
		SaxCallbackHandler handler = new SaxCallbackHandler();
		saxParser.parse(new InputSource(reader), handler);
		return handler.getRootTag();
	}

	private static class SaxCallbackHandler extends DefaultHandler {
		private DefaultXmlTag rootTag = null;
		private DefaultXmlTag currentTag;

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes)
				throws SAXException {
			DefaultXmlTag newTag = newXmlTag(qName, attributes);
			if (currentTag == null) {
				currentTag = newTag;
				rootTag = newTag;
			} else {
				currentTag.addChild(newTag);
				newTag.setParent(currentTag);
				currentTag = newTag;
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			currentTag.appendTextContent(ch, start, length);
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			currentTag = currentTag.getParentTag();
		}

		public IXmlTag getRootTag() {
			return rootTag;
		}

		private DefaultXmlTag newXmlTag(String tagName, Attributes attributes) {
			DefaultXmlTag newTag = new DefaultXmlTag(tagName);
			for (int i = 0; i < attributes.getLength(); ++i) {
				String key = attributes.getQName(i);
				String value = attributes.getValue(i);
				newTag.setAttribute(key, value);
			}
			return newTag;
		}
	}
}
