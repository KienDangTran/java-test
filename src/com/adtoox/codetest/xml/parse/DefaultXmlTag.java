package com.adtoox.codetest.xml.parse;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.IXmlTagMatcher;

import java.util.*;

class DefaultXmlTag implements IXmlTag {
	private String tagName;
	private StringBuilder textContent = null;
	private Map<String, String> attributes = new HashMap<String, String>();
	private List<IXmlTag> children = new ArrayList<IXmlTag>();
	private DefaultXmlTag parentTag;

	public DefaultXmlTag(String tagName) {
		this.tagName = tagName;
	}

	@Override
	public String getTagName() {
		return tagName;
	}

	protected void setParent(DefaultXmlTag parentTag) {
		this.parentTag = parentTag;
	}

	protected DefaultXmlTag getParentTag() {
		return parentTag;
	}

	@Override
	public String getAttribute(String name) {
		return attributes.get(name);
	}

	@Override
	public String getTextContent() {
		return textContent == null ? null : textContent.toString();
	}

	@Override
	public List<IXmlTag> getChildren() {
		return Collections.unmodifiableList(children);
	}

	@Override
	public List<IXmlTag> getChildren(IXmlTagMatcher matcher) {
		List<IXmlTag> filteredChildren = new ArrayList<IXmlTag>();
		for (IXmlTag child : children) {
			if (matcher.matches(child)) {
				filteredChildren.add(child);
			}
		}
		return Collections.unmodifiableList(filteredChildren);
	}

	protected void setAttribute(String key, String value) {
		attributes.put(key, value);
	}

	protected void addChild(DefaultXmlTag childTag) {
		children.add(childTag);
	}

	protected void appendTextContent(char[] ch, int start, int length) {
		if (this.textContent == null) {
			this.textContent = new StringBuilder();
		}
		this.textContent.append(ch, start, length);
	}
}
