package com.adtoox.codetest.xml.parse;

import com.adtoox.codetest.xml.IXmlTag;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.Reader;

public interface IXmlParser {
	public IXmlTag parse(Reader reader) throws SAXException, IOException;
}
