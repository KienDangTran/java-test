package com.adtoox.codetest.xml.print.bind;

import com.adtoox.codetest.xml.print.IPrintedPage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.List;

public class BookBinder implements IBookBinder {
	private static final Charset UTF_8 = Charset.forName("UTF-8");

	private File outputFolder;
	private DecimalFormat pageNumberFormatter = new DecimalFormat();

	public BookBinder(File outputFolder) {
		if (!outputFolder.exists()) {
			throw new IllegalArgumentException(outputFolder.getAbsolutePath() + " does not exist.");
		}
		if (!outputFolder.isDirectory()) {
			throw new IllegalArgumentException(outputFolder.getAbsolutePath() + " is not a directory.");
		}
		this.outputFolder = outputFolder;
	}


	@Override
	public void bindBook(List<IPrintedPage> pages) throws IOException {
		int number = 1;
		int maxNumberLength = String.valueOf(pages.size()).length();

		pageNumberFormatter.setMinimumIntegerDigits(maxNumberLength);

		for (IPrintedPage page : pages) {
			File pageOutput = new File(outputFolder, "page" + pageNumberFormatter.format(number) + ".txt");
			number++;
			FileOutputStream pageOutputStream = new FileOutputStream(pageOutput);
			try {
				OutputStreamWriter writer = new OutputStreamWriter(pageOutputStream, UTF_8);
				writer.write(page.toString());
				writer.flush();
			} finally {
				pageOutputStream.close();
			}
		}
	}

}
