package com.adtoox.codetest.xml.print.bind;

import com.adtoox.codetest.xml.print.IPrintedPage;

import java.io.IOException;
import java.util.List;

public interface IBookBinder {
	public void bindBook(List<IPrintedPage> pages) throws IOException;
}
