package com.adtoox.codetest.xml.print;

public interface IPrintedPage {
	public void add(int row, int column, String text);
}
