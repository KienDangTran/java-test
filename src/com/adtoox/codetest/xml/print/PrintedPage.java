package com.adtoox.codetest.xml.print;

import java.util.ArrayList;
import java.util.List;

public class PrintedPage implements IPrintedPage {
	private List<String> rows = new ArrayList<String>();

	public PrintedPage(int numberOfRows, int numberOfColumns) {
		for (int i = 0; i < numberOfRows; ++i) {
			StringBuilder row = new StringBuilder();
			for (int j = 0; j < numberOfColumns; ++j) {
				row.append(" ");
			}
			rows.add(row.toString());
		}
	}

	@Override
	public void add(int rowNumber, int columnNumber, String text) {
		String row = rows.get(rowNumber);
		StringBuilder newRow = new StringBuilder();
		newRow.append(row.substring(0, columnNumber));
		newRow.append(text);
		newRow.append(row.substring(columnNumber + text.length()));
		rows.set(rowNumber, newRow.toString());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		boolean first = true;
		for (String row : rows) {
			if (first) {
				first = false;
			} else {
				builder.append(System.getProperty("line.separator"));
			}
			builder.append(row);
		}
		return builder.toString();
	}

}
