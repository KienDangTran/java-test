package com.adtoox.codetest.xml;

public interface IXmlTagMatcher {
	public boolean matches(IXmlTag xmlTag);
}
