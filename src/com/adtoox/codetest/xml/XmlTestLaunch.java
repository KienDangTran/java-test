package com.adtoox.codetest.xml;

import com.adtoox.codetest.xml.book.BookBuilder;
import com.adtoox.codetest.xml.interpret.IXmlBookInterpreter;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;
import com.adtoox.codetest.xml.interpret.standard.XmlBookInterpreter;
import com.adtoox.codetest.xml.parse.XmlParser;
import com.adtoox.codetest.xml.print.IPrintedPage;
import com.adtoox.codetest.xml.print.bind.BookBinder;
import com.adtoox.codetest.xml.print.bind.IBookBinder;
import com.adtoox.codetest.xml.util.FileUtil;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class XmlTestLaunch {
	public static void main(String[] args)
			throws ParserConfigurationException, SAXException, IOException, XmlBookSyntaxException {
		File inputFile = new File("Story.xml");

		IXmlTag bookRootTag = FileUtil.parseFile(new XmlParser(), inputFile);

		Map<Object, Object> bookProperties =
				loadBookProperties(new File(FileUtil.getFileNameWithoutSuffix(inputFile).concat(".properties")));
		BookBuilder bookBuilder = new BookBuilder();

		IXmlBookInterpreter interpreter = new XmlBookInterpreter(bookProperties);
		interpreter.interpret(bookRootTag, bookBuilder);

		String outputFolderName = FileUtil.getFileNameWithoutSuffix(inputFile);
		File outputFolder = getCleanFolder(outputFolderName);

		IBookBinder bookBinder = new BookBinder(outputFolder);
		List<IPrintedPage> pages = bookBuilder.render();
		bookBinder.bindBook(pages);
	}


	private static File getCleanFolder(String foldername) throws IOException {
		File folder = new File(new File("").getAbsoluteFile(), foldername);
		cleanUpFolder(folder);
		return folder;
	}


	private static void cleanUpFolder(File folder) throws IOException {
		if (folder.exists()) {
			FileUtil.deleteRecursively(folder);
		}
		folder.mkdirs();
	}

	private static Map<Object, Object> loadBookProperties(File file) throws IOException {
		Map<Object, Object> properties = new Properties();
		if (file.exists() && file.isFile()) {
			properties = FileUtil.loadPropertiesFromFile(file);
		}

		return properties;
	}
}
