package com.adtoox.codetest.xml.interpret.standard;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.interpret.handler.ITagHandler;

public interface ITagHandlerProvider {
	public ITagHandler getHandler(IXmlTag xmlTag);
}
