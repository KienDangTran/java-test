package com.adtoox.codetest.xml.interpret.standard;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.book.TagConstant;
import com.adtoox.codetest.xml.interpret.IXmlBookInterpreter;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;
import com.adtoox.codetest.xml.interpret.handler.*;

import java.util.Map;

import static com.adtoox.codetest.xml.book.TagConstant.BOOK;

public class XmlBookInterpreter implements IXmlBookInterpreter {
	private TagHandlerProvider tagHandlerProvider;

	public XmlBookInterpreter(Map<Object, Object> bookProperties) {
		tagHandlerProvider = new TagHandlerProvider();
		tagHandlerProvider.registerHandler(new BookHandler(TagConstant.BOOK, tagHandlerProvider));
		tagHandlerProvider.registerHandler(new BreakHandler(TagConstant.BREAK, tagHandlerProvider));
		tagHandlerProvider.registerHandler(
				new ChapterHandler(TagConstant.CHAPTER, tagHandlerProvider, bookProperties));
		tagHandlerProvider.registerHandler(new EmptyLineHandler(TagConstant.EMPTY, tagHandlerProvider));
		tagHandlerProvider.registerHandler(new ListHandler(TagConstant.LIST, tagHandlerProvider));
		tagHandlerProvider.registerHandler(new ItemHandler(TagConstant.ITEM, tagHandlerProvider, bookProperties));
		tagHandlerProvider.registerHandler(new SectionHandler(TagConstant.SECTION, tagHandlerProvider));
		tagHandlerProvider.registerHandler(new TextHandler(TagConstant.TEXT, tagHandlerProvider, bookProperties));
	}

	@Override
	public void interpret(IXmlTag xmlRootTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException {
		if (!BOOK.equals(xmlRootTag.getTagName())) {
			throw new XmlBookSyntaxException("Expected \"" + BOOK + "\" as root tag. Got \"" + xmlRootTag + "\".");
		}
		tagHandlerProvider.getHandler(xmlRootTag).handle(xmlRootTag, bookBuilder);
	}
}
