package com.adtoox.codetest.xml.interpret.standard;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.interpret.handler.ITagHandler;
import com.adtoox.codetest.xml.interpret.handler.TagHandler;

import java.util.HashMap;
import java.util.Map;

public class TagHandlerProvider implements ITagHandlerProvider {
	Map<String, ITagHandler> tagHandlers = new HashMap<>();

	@Override
	public ITagHandler getHandler(IXmlTag xmlTag) {
		String tagName = xmlTag.getTagName();
		return tagHandlers.get(tagName);
	}

	public void registerHandler(TagHandler tagHandler) {
		this.tagHandlers.put(tagHandler.getHandledTagName(), tagHandler);
	}
}
