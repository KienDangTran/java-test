package com.adtoox.codetest.xml.interpret;

public class XmlBookSyntaxException extends Exception {
	private static final long serialVersionUID = 6307897140961470542L;

	public XmlBookSyntaxException() {
		super();
	}

	public XmlBookSyntaxException(String message, Throwable cause) {
		super(message, cause);
	}

	public XmlBookSyntaxException(String message) {
		super(message);
	}

	public XmlBookSyntaxException(Throwable cause) {
		super(cause);
	}

}
