package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;

public interface ITagHandler {
	public void handle(IXmlTag xmlTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException;
}
