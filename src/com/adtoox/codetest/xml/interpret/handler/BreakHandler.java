package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;
import com.adtoox.codetest.xml.interpret.standard.ITagHandlerProvider;

public class BreakHandler extends TagHandler {
	public BreakHandler(String handledTagName, ITagHandlerProvider tagHandlerProvider) {
		super(handledTagName, tagHandlerProvider);
	}

	@Override
	protected void handleSelf(IXmlTag textTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException {
		bookBuilder.addBreak();
	}
}
