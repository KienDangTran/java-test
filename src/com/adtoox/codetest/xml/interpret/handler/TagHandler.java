package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;
import com.adtoox.codetest.xml.interpret.standard.ITagHandlerProvider;

public abstract class TagHandler extends AbstractTagHandler {
	private String handledTagName;
	private ITagHandlerProvider tagHandlerProvider;

	public TagHandler(String handledTagName, ITagHandlerProvider tagHandlerProvider) {
		this.handledTagName = handledTagName;
		this.tagHandlerProvider = tagHandlerProvider;
	}

	protected ITagHandler getHandler(IXmlTag xmlTag) {
		return this.tagHandlerProvider.getHandler(xmlTag);
	}

	protected boolean allowedInside(String tagName) {
		return true;
	}

	protected int getRequiredInt(IXmlTag xmlTag, String attributeName) throws XmlBookSyntaxException {
		return Integer.parseInt(getRequiredAttribute(xmlTag, attributeName));
	}

	protected String getRequiredAttribute(IXmlTag xmlTag, String attributeName) throws XmlBookSyntaxException {
		String attributeValue = xmlTag.getAttribute(attributeName);
		if (attributeValue == null) {
			throw new XmlBookSyntaxException(
					"Tag " + xmlTag.getTagName() + " is missing required attribute \"" + attributeName + "\".");
		}
		return attributeValue;
	}

	protected abstract void handleSelf(IXmlTag xmlRootTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException;

	public String getHandledTagName() {
		return handledTagName;
	}
}
