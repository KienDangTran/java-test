package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;
import com.adtoox.codetest.xml.interpret.standard.ITagHandlerProvider;

public class ListHandler extends TagHandler {
	public ListHandler(String handledTagName, ITagHandlerProvider tagHandlerProvider) {
		super(handledTagName, tagHandlerProvider);
	}

	@Override
	protected void handleSelf(IXmlTag listTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException {
		String listBullet = this.getRequiredAttribute(listTag, "bullet");
		bookBuilder.setListBullet(listBullet);
	}
}
