package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.interpret.standard.ITagHandlerProvider;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class PlaceholderHandler extends TagHandler {
	Map<Object, Object> bookProperties = new HashMap<>();

	public PlaceholderHandler(
			String handledTagName,
			ITagHandlerProvider tagHandlerProvider,
			Map<Object, Object> bookProperties
	) {
		super(handledTagName, tagHandlerProvider);
		this.bookProperties = bookProperties;
	}

	protected String replacePlaceholder(String text) {
		if (this.bookProperties.size() == 0) {
			return text;
		}

		Pattern pattern = Pattern.compile("\\$\\{(\\w*)}");
		Matcher matcher = pattern.matcher(text);
		StringBuffer result = new StringBuffer();

		while (matcher.find()) {
			if (this.bookProperties.keySet().contains(matcher.group(1))) {
				matcher.appendReplacement(result, this.bookProperties.get(matcher.group(1)).toString());
			}
		}
		matcher.appendTail(result);

		return result.toString();
	}
}
