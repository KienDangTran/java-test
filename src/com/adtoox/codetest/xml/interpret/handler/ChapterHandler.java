package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;
import com.adtoox.codetest.xml.interpret.standard.ITagHandlerProvider;

import java.util.Map;

public class ChapterHandler extends PlaceholderHandler {
	public ChapterHandler(
			String handledTagName,
			ITagHandlerProvider tagHandlerProvider,
			Map<Object, Object> bookProperties
	) {
		super(handledTagName, tagHandlerProvider, bookProperties);
	}

	@Override
	protected void handleSelf(IXmlTag chapterTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException {
		String chapterName = this.getRequiredAttribute(chapterTag, "header");
		bookBuilder.newChapter(this.replacePlaceholder(chapterName));
	}

	@Override
	protected boolean allowedInside(String tagName) {
		return "book".equals(tagName);
	}
}
