package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;
import com.adtoox.codetest.xml.interpret.standard.ITagHandlerProvider;

import java.util.Map;

public class ItemHandler extends TextHandler {
	public ItemHandler(
			String handledTagName,
			ITagHandlerProvider tagHandlerProvider,
			Map<Object, Object> bookProperties
	) {
		super(handledTagName, tagHandlerProvider, bookProperties);
	}

	@Override
	protected void handleSelf(IXmlTag textTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException {
		String text = textTag.getTextContent();
		text = replacePlaceholder(text);
		text = formatText(text);
		bookBuilder.addListItem(text);
	}

	@Override
	protected boolean allowedInside(String tagName) {
		return "list".equals(tagName);
	}
}
