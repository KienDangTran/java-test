package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;

public abstract class AbstractTagHandler implements ITagHandler {
	@Override
	public final void handle(IXmlTag xmlTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException {
		handleSelf(xmlTag, bookBuilder);
		handleChildren(xmlTag, bookBuilder);
	}

	private void handleChildren(IXmlTag xmlTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException {
		for (IXmlTag childTag : xmlTag.getChildren()) {
			ITagHandler handler = getHandler(childTag);
			if (handler != null) {
				handler.handle(childTag, bookBuilder);
			} else {
				onUnrecognizedTagName(xmlTag, childTag.getTagName());
			}
		}
	}

	protected void onUnrecognizedTagName(IXmlTag parent, String unrecognizedTagName) throws XmlBookSyntaxException {
		throw new XmlBookSyntaxException(
				"Tag <" + unrecognizedTagName + "> not supported within <" + parent.getTagName() + ">.");
	}

	protected abstract void handleSelf(IXmlTag xmlTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException;

	protected abstract ITagHandler getHandler(IXmlTag xmlTag);
}
