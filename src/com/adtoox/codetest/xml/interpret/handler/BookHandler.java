package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;
import com.adtoox.codetest.xml.interpret.standard.ITagHandlerProvider;


public class BookHandler extends TagHandler {
	public BookHandler(String handledTagName, ITagHandlerProvider tagHandlerProvider) {
		super(handledTagName, tagHandlerProvider);
	}

	@Override
	protected void handleSelf(IXmlTag xmlRootTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException {
		int pageWidth = this.getRequiredInt(xmlRootTag, "pageWidth");
		int pageHeight = this.getRequiredInt(xmlRootTag, "pageHeight");

		bookBuilder.setPageSize(pageWidth, pageHeight);
	}
}
