package com.adtoox.codetest.xml.interpret.handler;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;
import com.adtoox.codetest.xml.interpret.XmlBookSyntaxException;
import com.adtoox.codetest.xml.interpret.standard.ITagHandlerProvider;

public class SectionHandler extends TagHandler {
	public SectionHandler(String handledTagName, ITagHandlerProvider tagHandlerProvider) {
		super(handledTagName, tagHandlerProvider);
	}

	@Override
	protected void handleSelf(IXmlTag sectionTag, IBookBuilder bookBuilder) throws XmlBookSyntaxException {
		bookBuilder.newSection();
	}

	@Override
	protected boolean allowedInside(String tagName) {
		return "chapter".equals(tagName);
	}
}
