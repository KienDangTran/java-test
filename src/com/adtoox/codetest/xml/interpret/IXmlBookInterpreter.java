package com.adtoox.codetest.xml.interpret;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.book.IBookBuilder;

public interface IXmlBookInterpreter {
	public void interpret(IXmlTag xml, IBookBuilder bookBuilder) throws XmlBookSyntaxException;
}
