package com.adtoox.codetest.xml;

import java.util.List;


public interface IXmlTag {
	public String getTagName();

	public String getAttribute(String name);

	public String getTextContent();

	public List<IXmlTag> getChildren();

	public List<IXmlTag> getChildren(IXmlTagMatcher matcher);
}
