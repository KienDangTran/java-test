package com.adtoox.codetest.xml.book;

import com.adtoox.codetest.xml.print.IPrintedPage;
import com.adtoox.codetest.xml.print.PrintedPage;

import java.util.ArrayList;
import java.util.List;

public class BookBuilder implements IBookBuilder {
	private static final String SECTION_INDENT = "      ";

	private boolean setupStage = true;
	private int maxCharactersPerRow = 46;
	private int maxLinesPerPage = 40;

	private List<Chapter> chapters = new ArrayList<Chapter>();

	private String listBullet = "*";

	private static String getSpaces(int spaces) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < spaces; ++i) {
			stringBuilder.append(" ");
		}
		return stringBuilder.toString();
	}

	@Override
	public void setPageSize(int columns, int rows) {
		if (!setupStage) {
			throw new IllegalStateException("setPageSize must be called before any builder methods are called.");
		}
		maxCharactersPerRow = columns;
		maxLinesPerPage = rows;
	}

	@Override
	public void newChapter(String header) {
		setupStage = false;
		Chapter chapter = new Chapter();
		chapters.add(chapter);

		Page page = new Page();

		chapter.addPage(page);

		page.addHeader(header);
	}

	@Override
	public void addText(String text) {
		addText("", text);
	}

	private void addText(String indention, String text) {
		setupStage = false;
		List<String> words = breakUpIntoWords(text);
		while (words.size() > 0) {
			Page current = getCurrentPage();
			words = current.addWordsIndented(indention, words);
		}
	}

	@Override
	public void setListBullet(String bullet) {
		this.listBullet = bullet;
	}

	@Override
	public void addListItem(String text) {
		setupStage = false;

		String currentRow = getCurrentPage().getCurrentRow();
		if (currentRow.length() != 0) {
			getCurrentPage().newRow();
		}
		getCurrentPage().setCurrentRow(listBullet);
		addText(getSpaces(listBullet.length() + 1), text);
	}

	private Chapter getCurrentChapter() {
		return chapters.get(chapters.size() - 1);
	}

	private Page getCurrentPage() {
		Chapter currentChapter = getCurrentChapter();
		return currentChapter.pages.get(currentChapter.pages.size() - 1);
	}

	private List<String> breakUpIntoWords(String text) {
		List<String> words = new ArrayList<String>();
		for (String word : text.split(" ")) {
			words.add(word);
		}
		return words;
	}

	@Override
	public void addBreak() {
		setupStage = false;
		getCurrentPage().newRow();
	}

	@Override
	public void newSection() {
		setupStage = false;
		String currentRow = getCurrentPage().getCurrentRow();
		if (currentRow.length() != 0) {
			getCurrentPage().newRow();
		}
		getCurrentPage().setCurrentRow(SECTION_INDENT);
	}

	@Override
	public void addEmptyLine() {
		setupStage = false;
		String line = getCurrentPage().getCurrentRow();
		addBreak();
		if (line.length() > 0) {
			addBreak();
		}
	}

	public List<IPrintedPage> render() {
		List<IPrintedPage> printed = new ArrayList<IPrintedPage>();
		for (Chapter chapter : chapters) {
			for (Page page : chapter.pages) {
				IPrintedPage printedPage = new PrintedPage(maxLinesPerPage + 1, maxCharactersPerRow);

				int rowNumber = 0;
				for (String row : page.rows) {
					printedPage.add(rowNumber++, 0, row);
				}
				String pageNumberText = " page " + String.valueOf(page.getPageNumber());
				printedPage.add(maxLinesPerPage, maxCharactersPerRow - pageNumberText.length(), pageNumberText);

				printed.add(printedPage);
			}
		}
		return printed;
	}

	private class Chapter {
		private List<Page> pages = new ArrayList<Page>();

		public void addPage(Page page) {
			pages.add(page);
		}
	}

	private class Page {
		private List<String> rows = new ArrayList<String>();

		public Page() {
			rows.add("");
		}

		public List<String> addWordsIndented(String indention, List<String> words) {
			List<String> notFitted = new ArrayList<String>();
			boolean restOfWordsNotFitted = false;
			for (String originalWord : words) {
				if (restOfWordsNotFitted) {
					notFitted.add(originalWord);
				} else {
					boolean spaceNeeded = getCurrentRow().length() > 0;
					String word = originalWord;
					if (spaceNeeded) {
						word = " " + word;
					}
					int wordLength = word.length();

					if (wordLength > maxCharactersPerRow) {
						throw new RuntimeException(
								"Too short row (max " + maxCharactersPerRow + " per row)! Or possibly to long word ("
										+ originalWord + ")!");
					} else {
						if (wordLength <= getLeftOnCurrentRow()) {
							setCurrentRow(getCurrentRow() + word);
						} else {
							if (rows.size() < maxLinesPerPage) {
								rows.add(indention + originalWord);
							} else {
								notFitted.add(originalWord);
								restOfWordsNotFitted = true;
								getCurrentChapter().addPage(new Page());
							}
						}
					}
				}
			}
			return notFitted;
		}

		public void addHeader(String header) {
			int paddingLeft = (maxCharactersPerRow - header.length()) / 2;
			int paddingRight = maxCharactersPerRow - header.length() - paddingLeft;
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < paddingLeft; ++i) {
				builder.append("_");
			}
			builder.append(header);
			for (int i = 0; i < paddingRight; ++i) {
				builder.append("_");
			}
			setCurrentRow(builder.toString());
			rows.add("");
			rows.add("");
		}

		public int getPageNumber() {
			int pageNumber = 1;
			for (Chapter chapter : chapters) {
				for (Page page : chapter.pages) {
					if (page != this) {
						pageNumber++;
					} else {
						return pageNumber;
					}
				}
			}
			throw new RuntimeException("Cannot find page.");
		}

		public void newRow() {
			if (rows.size() < maxLinesPerPage) {
				rows.add("");
			} else {
				getCurrentChapter().addPage(new Page());
			}
		}

		public int getLeftOnCurrentRow() {
			return maxCharactersPerRow - getCurrentRow().length();
		}

		private String getCurrentRow() {
			if (rows.size() == 0) {
				rows.add("");
			}
			return rows.get(rows.size() - 1);
		}

		private void setCurrentRow(String text) {
			rows.set(rows.size() - 1, text);
		}


	}
}
