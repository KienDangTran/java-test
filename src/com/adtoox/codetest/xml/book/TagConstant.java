package com.adtoox.codetest.xml.book;

public class TagConstant {
	public static final String BOOK = "book";
	public static final String CHAPTER = "chapter";
	public static final String SECTION = "section";
	public static final String EMPTY = "empty";
	public static final String TEXT = "text";
	public static final String BREAK = "break";
	public static final String ITEM = "item";
	public static final String LIST = "list";
}
