package com.adtoox.codetest.xml.book;


public interface IBookBuilder {
	public void setPageSize(int columns, int rows);

	public void newChapter(String header);

	public void addText(String text);

	public void addBreak();

	public void newSection();

	public void addEmptyLine();

	public void addListItem(String text);

	public void setListBullet(String bullet);
}
