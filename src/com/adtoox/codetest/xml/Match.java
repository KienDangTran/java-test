package com.adtoox.codetest.xml;

public class Match {
	public static IXmlTagMatcher byName(String name) {
		return new NameMatcher(name);
	}

	private static class NameMatcher implements IXmlTagMatcher {
		private String name;

		public NameMatcher(String name) {
			this.name = name;
		}

		@Override
		public boolean matches(IXmlTag xmlTag) {
			return name != null && name.equals(xmlTag.getTagName());
		}
	}
}
