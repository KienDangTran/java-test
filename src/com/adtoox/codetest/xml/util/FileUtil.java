package com.adtoox.codetest.xml.util;

import com.adtoox.codetest.xml.IXmlTag;
import com.adtoox.codetest.xml.parse.IXmlParser;
import org.xml.sax.SAXException;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Properties;

public class FileUtil {
	public static IXmlTag parseFile(IXmlParser parser, File data) throws SAXException, IOException {
		Reader reader = new InputStreamReader(new FileInputStream(data), Charset.forName("UTF-8"));
		try {
			return parser.parse(reader);
		} finally {
			reader.close();
		}
	}

	public static void deleteRecursively(File file) throws IOException {
		if (file.isDirectory()) {
			for (File subFile : file.listFiles()) {
				deleteRecursively(subFile);
			}
		}
		if (!file.delete()) {
			throw new IOException("Failed to delete " + file.getAbsolutePath() + ".");
		}
	}

	public static String getFileNameWithoutSuffix(File file) {
		String completeFilename = file.getName();
		int indexOfSuffixDelimiter = completeFilename.indexOf(".");
		if (indexOfSuffixDelimiter == -1) {
			return completeFilename;
		} else {
			return completeFilename.substring(0, indexOfSuffixDelimiter);
		}
	}

	public static Map<Object, Object> loadPropertiesFromFile(File file) throws IOException {
		Properties properties = new Properties();
		InputStream input = null;

		try {
			input = new FileInputStream(file);
			properties.load(input);
		} finally {
			if (input != null) {
				input.close();
			}
		}

		return properties;
	}
}
